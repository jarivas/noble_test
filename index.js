const noble = require('noble');

noble.on('stateChange', state => {
    if (state === 'poweredOn') {
      console.log('Scanning');
      noble.startScanning();
    } else {
      noble.stopScanning();
    }
  });

  noble.on('discover', peripheral => {
    // connect to the first peripheral that is scanned
    console.log(peripheral);
});
